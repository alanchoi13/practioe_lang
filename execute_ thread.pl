
use strict;
use warnings FATAL => 'all';


use Thread qw(async);

$thr = async {
    foreach (1..10) {
        print "$_ line in thread￦n";
    }
};