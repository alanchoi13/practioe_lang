#!/usr/bin/perl
use strict;
use warnings FATAL => 'all';
use Data::Dumper qw(Dumper);
use Cwd qw(abs_path);



my $d4j_home = $ENV{"D4J_HOME"};
my $defects4j_bin = $d4j_home."/framework/bin/";
my $p_n = "Math";
my $p_n_l = "math";

foreach my $count (1 .. 106){
	print "======================= start ".$p_n." checkout b".$count." =====================\n";
	my $cmd_basic = "defects4j-fluccs 0 checkout -p ".$p_n." -v ".$count."b -w ".$p_n_l."_".$count."_b";
	my $cmd_location = $defects4j_bin.$cmd_basic;
	system($cmd_location);
	print "======================= end ".$p_n." checkout b".$count." =====================\n";
}

foreach my $count (1 .. 106){
	print "======================= start ".$p_n." prepare b".$count." =====================\n";
	my $cmd_prepare = "defects4j-fluccs 1 fluccs-prepare -p ".$p_n." -b ".$count." -w ".$p_n_l."_".$count."_b -c 1";
	my $cmd_location = $defects4j_bin.$cmd_prepare;
	system($cmd_location);
	print "======================= end ".$p_n." prepare b".$count." =====================\n";
}
