#!/usr/bin/perl
use strict;
use warnings FATAL => 'all';
use Data::Dumper qw(Dumper);
use Cwd qw(abs_path);



my $d4j_home = $ENV{"D4J_HOME"};
my $defects4j_bin = $d4j_home."/framework/bin/";
my $p_n = "Closure";
my $p_n_l = "closure";

foreach my $count (1 .. 133){
	print "======================= start ".$p_n." codeAndchange b".$count." =====================\n";
	my $cmd_prepare = "defects4j-fluccs 1 fluccs-codeAndchange -p ".$p_n." -b ".$count." -w ".$p_n_l."_".$count."_b";
	my $cmd_location = $defects4j_bin.$cmd_prepare;
	system($cmd_location);
	print "======================= end ".$p_n." codeAndchange b".$count." =====================\n";
}

foreach my $count (1 .. 133){
	print "======================= start ".$p_n." complexity b".$count." =====================\n";
	my $cmd_prepare = "defects4j-fluccs 1 fluccs-complexity -p ".$p_n." -b ".$count." -w ".$p_n_l."_".$count."_b";
	my $cmd_location = $defects4j_bin.$cmd_prepare;
	system($cmd_location);
	print "======================= end ".$p_n." complexity b".$count." =====================\n";
}

foreach my $count (1 .. 133){
	print "======================= start ".$p_n." gather b".$count." =====================\n";
	my $cmd_prepare = "defects4j-fluccs 1 fluccs-gather -p ".$p_n." -b ".$count." -w ".$p_n_l."_".$count."_b -g 0 -n 1 -d";
	my $cmd_location = $defects4j_bin.$cmd_prepare;
	system($cmd_location);
	print "======================= end ".$p_n." gather b".$count." =====================\n";
}


