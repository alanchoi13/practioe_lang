#!/usr/bin/perl

### reference
# https://stackoverflow.com/questions/1045792/how-can-i-list-all-of-the-files-in-a-directory-with-perl
###

use strict;
use warnings FATAL => 'all';
use Data::Dumper qw(Dumper);
use File::Basename;
my $directory = '/home/choi/dataD/lang_2_b';

my $directory2 = '/home/choi';

### use opendir : extract filename
opendir my $dir, "/home/choi" or die "Cannot open directory: $!";
my @dir_files = readdir $dir;
closedir $dir;

print Dumper \@dir_files;


### use glob : extract directory location
my @files = glob( $directory.'/*.csv' );
print Dumper \@files;

#while (my $f = readdir(DIR)){
#    print "$f\n";
#}
#my @basename_files = basename(@files);


my @diretoryname = glob($directory2.'/*');

print Dumper \@diretoryname;