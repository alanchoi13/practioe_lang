use strict;
use warnings FATAL => 'all';
use Data::Dumper qw(Dumper);
use Cwd qw(abs_path);



my $d4j_home = $ENV{"D4J_HOME"};
my $defects4j_bin = $d4j_home."/framework/bin/";


#foreach my $count (1 .. 65){
#		print "======================= start Lang checkout b".$count." =====================\n";
#		my $cmd_basic = "defects4j-fluccs 0 checkout -p Lang -v ".$count."b -w lang_".$count."_b";
#		my $cmd_location = $defects4j_bin.$cmd_basic;
#		system($cmd_location);
#		print "======================= end Lang checkout b".$count." =====================\n";
#}

foreach my $count (81 .. 90){
	print "======================= start Closure coverage b".$count." =====================\n";
	my $cmd_prepare = "defects4j-fluccs 1 fluccs-coverage -p Closure -b ".$count." -w closure_".$count."_b -c 1";
	my $cmd_location = $defects4j_bin.$cmd_prepare;
	system($cmd_location);
	print "======================= end Closure coverage b".$count." =====================\n";
}
