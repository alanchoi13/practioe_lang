#!/usr/bin/perl
use strict;
use warnings FATAL => 'all';
use JSON;
use Data::Dumper qw(Dumper);
use File::Basename;
use List::MoreUtils qw(uniq);
use Array::Unique;
use JSON;
use File::Basename;
my %hash1 = (
    "1" => 0,
    "2" => 0,
    "3" => 0,
    "4" => 0,
    "5" => 0,
);

my %hash2 = (
    "2"      => '1',
    "3"      => '1',
    "5"      => '1',
    "result" => '0',
);

my $dir_env = $ENV{"D4J_HOME"};
my $dir_name = $dir_env.'/framework/bin/fluccs/method_stmt/Lang';

### get csv files info.
my @lang_files_csv = glob($dir_name.'/stmt_mth_id_Lang_*.csv');
#my @lang_files_csv_names;
#push(@lang_files_csv_names, basename(glob($dir_name.'/stmt_mth_id_Lang_*.csv')));
print Dumper \@lang_files_csv;