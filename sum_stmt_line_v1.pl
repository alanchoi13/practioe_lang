use strict;
use warnings FATAL => 'all';
use JSON;
use Data::Dumper qw(Dumper);
use File::Basename;
use List::MoreUtils qw(uniq);
use Array::Unique;
use JSON;
use File::Basename;


my $dir_env = $ENV{"D4J_HOME"};
my $dir_name = $dir_env.'/framework/bin/fluccs/method_stmt/Lang';

### get csv files info.
my @lang_files_csv = glob($dir_name.'/stmt_mth_id_Lang_*.csv');
#my @lang_files_csv_names;
#push(@lang_files_csv_names, basename(glob($dir_name.'/stmt_mth_id_Lang_*.csv')));
print Dumper \@lang_files_csv;
#print Dumper \@lang_files_csv_names;

my $field1;
my $field2;
my $field3;
my %total_hash;
my %class_hash;
my %line_hash;

foreach my $file (@lang_files_csv){
    my %arr_to_hash;
    my @array;
    open (F, $file) || die ("Could not open $file!");
    while (my $line = <F>)
    {

        ($field1,$field2,$field3) = split ',', $line;
        #      print "$field1 : $field2 : $field3 :";
        #        push( @array, $field2 );
        #        push( @ { $class_hash { $field1 } }, $field2 );
        #        push( @ { $class_hash { $field1 } }, {$field2 => '0'} );
        $class_hash{$field1}{$field2} = '0';
        #        $line_hash{$field2} = '0';
        #        $class_hash{$field1} =%line_hash;
        #        $TEST{ $field1 } = $field2;
        #        print Dumper \%class_hash;
    }
    #print Dumper \%class_hash;
    #    print basename($file);
    #    %arr_to_hash = map { $_ => 0 } @array;
    #    %total_hash = (%total_hash, %arr_to_hash);
    #    %total_hash = (%total_hash, %class_hash);
    #    print encode_json \@array,"\n";
    #    open my $fh, ">", "dataD/"."hit-".basename($file)."json";
    ##    print $fh encode_json(\%arr_to_hash);
    #    print $fh encode_json(\%class_hash);
    ##    close $fh;
    #    close (F);
}

open my $fh, ">", "dataD/"."stmt_unique_line.json";
print $fh encode_json(\%class_hash);
close $fh;

open my $fh_dp, ">", "dataD/"."stmt_unique_line.dp";
print $fh_dp Dumper \%class_hash;
close $fh_dp;






















#my @fh;
#foreach (@lang_files_csv) {
#  open my $fh, '<', $_ or die "Unable to open '$_' for reading: $!";
#  push @fh, $fh;
#}
#
#
#
#### read @fh files to array
#my @data;
#while (grep { not eof } @fh) {
#    for my $fh (@fh) {
#        if (defined(my $line = <$fh>)) {
#            chomp $line;
#            my @split = split(/,/, $line);
#            push @data, \@split;
#        }
#    }
#    my @col = map $_->[ 1 ], @data;
#    print Dumper \@col;
#}
#print Dumper @data;

