#!/usr/bin/perl
use strict;
use warnings FATAL => 'all';

### unique
use List::MoreUtils qw(uniq);


use Data::Dumper qw(Dumper);



my @words = qw(foo bar baz foo zorg baz);

my @unique_words = uniq @words;

print Dumper \@unique_words;


