#!/usr/bin/perl
use strict;
use warnings FATAL => 'all';
use Cwd;
use Data::Dumper qw(Dumper);
use File::Spec;
### file_open

##unique
use List::MoreUtils qw(uniq);



my $file_name = 'stmt_mth_Lang_2.csv';
my $file_location = getcwd()."/dataD/".$file_name;
print $file_location."\n";
my @data;

open (my $fh, '<', $file_location) or die "Can't read file '$file_location' \n [$!] ";

while (my $line = <$fh>){
    chomp $line;
#    print Dumper $line;
    my @line_split = split(/,/, $line);
    push @data, \@line_split;


}

print $data[2][1];

##extract array want to col.
my @col = map $_->[ 1 ], @data;
print Dumper \@col;


##adapt unique function.
uniq @col;


##adapt unique function string ver.
my @sort_col = sort @col;
print Dumper \@sort_col;



##adapt unique number sort descending ver.
## using operator <=>
my @num_sort_col = sort { $a <=> $b } @col;
print Dumper \@num_sort_col;


