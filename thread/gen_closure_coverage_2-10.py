import subprocess
from multiprocessing import Process



def execute_closure(count):
    subprocess.call(
        "defects4j-fluccs 1 fluccs-coverage -p Closure -b " + str(count) + " -w closure_" + str(count) + "_b -c 1",
        shell=True)


if __name__ == '__main__':
    procs = []
    for count in (2, 11):
        proc = Process(target=execute_closure, args=(str(count)))
        procs.append(proc)
        proc.start()

    for proc in procs:
        proc.join()